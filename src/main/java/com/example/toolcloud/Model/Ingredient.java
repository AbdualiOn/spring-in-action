package com.example.toolcloud.Model;

import com.example.toolcloud.Model.Enum.Type;
import lombok.Data;

@Data
public class Ingredient {
    private final String id;
    private final String name;
    private final Type type;
}
