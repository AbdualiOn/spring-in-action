package com.example.toolcloud.Model.Enum;

public enum Type {
    WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
}
